
#include <algorithm>
#include <iostream>
#include <valarray>
#include <iterator>
#include <numeric>
#include <vector>
#include <chrono>
#include <random>
#include <cmath>

#include "matrix.h"
#include "reader.h"
#include "neural_network.h"

void test_streetlights() {
  std::valarray<double> weights = {0.5, 0.48, -0.7};
  double alpha = 0.1;
  std::valarray<std::valarray<double>> streetlights = {
    { 1, 0, 1 },
    { 0, 1, 1 },
    { 0, 0, 1 },
    { 1, 1, 1 },
    { 0, 1, 1 },
    { 1, 0, 1 }
  };

  std::valarray<double> walk_vs_stop = {0, 1, 0, 1, 1, 0};

  double eps = 1e-10;
  size_t iter_max = 1000;
  double error_for_all_lights = eps;

  for (size_t iter{}; iter < iter_max && error_for_all_lights >= eps; ++iter) {

    error_for_all_lights = 0;
    for (size_t row_index{}; row_index < walk_vs_stop.size(); ++row_index) {
      const auto& input = streetlights[row_index];
      const auto& goal_prediction = walk_vs_stop[row_index];

      auto prediction = std::inner_product(std::begin(input), std::end(input), std::begin(weights), 0.);

      auto error = std::pow(goal_prediction - prediction, 2.);
      error_for_all_lights += error;

      auto delta = prediction - goal_prediction;
      weights = weights - (alpha * (input * delta));
      std::cout << "prediction: " << prediction << std::endl;
    }

    std::cout << "weights: ";
    std::copy(std::begin(weights), std::end(weights), std::ostream_iterator<double>(std::cout, " "));
    std::cout << std::endl;
    std::cout << "iter: " << iter << std::endl;
    std::cout << "error: " << error_for_all_lights << std::endl << std::endl;
  }
}

void test_streetlights_3() {
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution;

  auto rand = [](double x) {
    static unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    static std::default_random_engine generator(seed);
    static std::uniform_real_distribution<double> distribution;
    return -1 + 2 * distribution(generator);
  };

  matrix_t streetlights = {
    3, {
      1, 0, 1,
      0, 1, 1,
      0, 0, 1,
      1, 1, 1,
    },
  };

  matrix_t walk_vs_stop = {
    1, {
      1,
      1,
      0,
      0,
    },
  };

  auto layer0 = matrix_t(1, 3);
  auto layer1 = matrix_t(1, 4);
  auto layer2 = matrix_t(1, 1);

  auto weights_0 = matrix_t(layer0.n(), layer1.n()).apply(rand);
  auto weights_1 = matrix_t(layer1.n(), layer2.n()).apply(rand);

  std::cout << "weights_0: " << weights_0.show() << std::endl;
  std::cout << "weights_1: " << weights_1.show() << std::endl;

  double eps = 1e-8;
  size_t iter_max = 10000;
  double layer2_error = eps;

  for (size_t iter{}; iter < iter_max && layer2_error >= eps; ++iter) {

    layer2_error = 0;
    for (size_t ind{}; ind < walk_vs_stop.m(); ++ind) {
      auto f0       = [](const matrix_t& matrix) {
        return matrix.apply([](double x) { return tanh(x); }); };
      auto f0_deriv = [](const matrix_t& matrix) {
        return matrix.apply([](double x) { return 1 - x * x; }); };

      auto square     = [](double x) { return x * x; };
      auto alpha_mult = [](double x) { return 0.1 * x; };

      auto layer0 = streetlights.row(ind);
      auto layer1 = f0(layer0 * weights_0);
      auto layer2 = layer1 * weights_1;

      auto goal_prediction = walk_vs_stop.row(ind);
      layer2_error += (layer2 - goal_prediction).apply(square).sum();

      auto layer2_delta = goal_prediction - layer2;
      auto layer1_delta = (layer2_delta * weights_1.t()).mult_nativ(f0_deriv(layer1));

      weights_1 = weights_1 + (layer1.t() * layer2_delta).apply(alpha_mult);
      weights_0 = weights_0 + (layer0.t() * layer1_delta).apply(alpha_mult);

      std::cout << "layer2: " << layer2.show() << std::endl;
      std::cout << "prediction: " << goal_prediction.show() << std::endl;
    }

    std::cout << std::endl;
    std::cout << "iter: " << iter << std::endl;
    std::cout << "layer2_error: " << layer2_error << std::endl << std::endl;
    std::cout << "weights_0: " << weights_0.show() << std::endl;
    std::cout << "weights_1: " << weights_1.show() << std::endl;
  }
}

void test_mnist() {
  auto rand = [](double x) {
    static unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    static std::default_random_engine generator(seed);
    static std::uniform_real_distribution<double> distribution;
    return 0.001 * (-1 + 2 * distribution(generator));
  };

  auto square = [](double x) { return x * x; };
  auto alpha_mult = [](double x) { return 0.001 * x; };

  reader_mnist_t reader_mnist;
  reader_mnist.init("./dataset/mnist");

  auto layer0_n = reader_mnist.images_training.front().pixels.size();
  auto layer1_n = 10;

  std::vector<matrix_t> images_training(reader_mnist.images_training.size(), matrix_t(1, layer0_n));
  for (size_t ind{}; ind < images_training.size(); ++ind) {
    for (size_t i{}; i < layer0_n; ++i) {
      images_training.at(ind).at(0, i) = reader_mnist.images_training.at(ind).pixels.at(i) / 255.; // normalization
    }
  }

  std::vector<matrix_t> images_training_label(reader_mnist.images_training.size(), matrix_t(1, layer1_n));
  for (size_t ind{}; ind < images_training.size(); ++ind) {
    for (size_t i{}; i < layer1_n; ++i) {
      images_training_label.at(ind).at(0, i) = 0;
    }
    images_training_label.at(ind).at(0, reader_mnist.images_training.at(ind).label) = 1;
  }

  std::vector<matrix_t> images_test(reader_mnist.images_test.size(), matrix_t(1, layer0_n));
  for (size_t ind{}; ind < images_test.size(); ++ind) {
    for (size_t i{}; i < layer0_n; ++i) {
      images_test.at(ind).at(0, i) = reader_mnist.images_test.at(ind).pixels.at(i) / 255.; // normalization
    }
  }

  double eps = 1e-1;
  size_t iter_max = 1000000;
  double error = eps;
  auto weights0 = matrix_t(layer0_n, layer1_n).apply(rand);

  for (size_t iter{}; iter < iter_max && error >= eps; ++iter) {

    error = 0;
    for (size_t ind{}; ind < images_training.size(); ++ind) {
      const auto& layer0 = images_training.at(ind);
      const auto& layer1_goal = images_training_label.at(ind);

      auto layer1 = layer0 * weights0;

      error += (layer1 - layer1_goal).apply(square).sum();
      auto delta = layer1_goal - layer1;

      weights0 = weights0 + (layer0.t() * delta).apply(alpha_mult);
    }

    std::cout << std::endl;
    std::cout << "iter: " << iter << std::endl;
    std::cout << "error: " << error << std::endl << std::endl;
  }

  { // test
    double success = 0;
    for (size_t ind{}; ind < images_test.size(); ++ind) {
      const auto& layer0 = images_test.at(ind);

      auto layer1 = layer0 * weights0;

      double max_v = -1;
      size_t max_i = 0;
      for (size_t i{}; i < layer1.n(); ++i) {
        if (layer1.at(0, i) > max_v) {
          max_v = layer1.at(0, i);
          max_i = i;
        }
      }

      if (max_v == reader_mnist.images_test.at(ind).label) {
        success += 1.;
      }
    }
    std::cout << "success: " << success / images_test.size() << std::endl;
  }
}

void test_mnist2(const std::string& name, size_t hidden_nodes, double alpha, double eps) {
  reader_mnist_t reader_mnist;
  reader_mnist.init("./dataset/mnist");

  const auto& images_training = reader_mnist.images_training;
  const auto& images_test = reader_mnist.images_test;
  assert(!images_training.empty());

  size_t images_count = images_training.size();
  size_t pixels_count = images_training.front().pixels.size();
  size_t labels_count = 10;
  size_t images_test_count = images_test.size();

  std::vector<Eigen::VectorXd> inputs_array(images_count, Eigen::VectorXd(pixels_count));
  std::vector<Eigen::VectorXd> targets_array(images_count, Eigen::VectorXd(labels_count));

  std::vector<Eigen::VectorXd> inputs_test_array(images_test_count, Eigen::VectorXd(pixels_count));
  std::vector<Eigen::VectorXd> targets_test_array(images_test_count, Eigen::VectorXd(labels_count));

  for (size_t ind{}; ind < images_count; ++ind) {
    for (size_t i{}; i < pixels_count; ++i) {
      inputs_array.at(ind)(i) = 0.01 + 0.98 * images_training.at(ind).pixels.at(i) / 256.;
    }
  }

  for (size_t ind{}; ind < images_count; ++ind) {
    for (size_t i{}; i < labels_count; ++i) {
      targets_array.at(ind)(i) = 0.01;
    }
    targets_array.at(ind)(images_training.at(ind).label) = 0.99;
  }

  for (size_t ind{}; ind < images_test_count; ++ind) {
    for (size_t i{}; i < pixels_count; ++i) {
      inputs_test_array.at(ind)(i) = 0.01 + 0.98 * images_test.at(ind).pixels.at(i) / 256.;
    }
  }

  for (size_t ind{}; ind < images_test_count; ++ind) {
    for (size_t i{}; i < labels_count; ++i) {
      targets_test_array.at(ind)(i) = 0.01;
    }
    targets_test_array.at(ind)(images_test.at(ind).label) = 0.99;
  }

  neural_network_t neural_network(name, pixels_count, hidden_nodes, labels_count, alpha);

  neural_network.train_dataset(eps, inputs_array, targets_array);

  size_t success{};
  for (size_t ind{}; ind < inputs_test_array.size(); ++ind) {
    auto prediction = neural_network.query(inputs_test_array.at(ind));
    Eigen::MatrixXf::Index row1, col1, row2, col2;
    prediction.maxCoeff(&row1, &col1);
    targets_test_array.at(ind).maxCoeff(&row2, &col2);
    success += (row1 == row2);
  }
  std::cout << "success: " << 1. * success / inputs_test_array.size() << std::endl;
}

int main(int argc, char* argv[]) {

  if (argc != 5) {
    std::cout << "./<program> <name> <hidden_nodes> <alpha> <eps>" << std::endl;
    return -1;
  }

  std::string name = argv[1];
  size_t hidden_nodes = std::stol(argv[2]);
  double alpha = std::stod(argv[3]);
  double eps = std::stod(argv[4]);

  test_mnist2(name, hidden_nodes, alpha, eps);

  return 0;
}



#include <deque>
#include <fstream>

struct reader_base_t {
  std::deque<char> content;

  void init(const std::string& path) {
    std::ifstream fin(path);
    content = std::deque<char>((std::istreambuf_iterator<char>(fin)), (std::istreambuf_iterator<char>()));
  }

  void read_base(void* value, size_t size) {
    if (content.size() < size) {
      throw std::runtime_error("invalid format");
    }
    std::copy(content.begin(), content.begin() + size, (char*) value);
    content.erase(content.begin(), content.begin() + size);
  }

  template <typename number_t>
  void read_number(number_t& value) {
    read_base(&value, sizeof(value));
    std::reverse((char*) &value, sizeof(value) + (char*) &value);
  }

  size_t size() {
    return content.size();
  }
};

struct reader_mnist_t {
  struct image_t {
    std::vector<uint8_t> pixels;
    uint8_t              label;
  };

  using images_t = std::vector<image_t>;

  images_t images_training;
  images_t images_test;

  void init(const std::string& path) {
    read_images(images_training, path + "/train-images-idx3-ubyte");
    read_images(images_test,     path + "/t10k-images-idx3-ubyte");
    read_images_label(images_training, path + "/train-labels-idx1-ubyte");
    read_images_label(images_test,     path + "/t10k-labels-idx1-ubyte");
  }

  void read_images(images_t& images, const std::string& path) {
    reader_base_t reader_base;
    reader_base.init(path);

    uint32_t magic_number;
    reader_base.read_number(magic_number);
    // TODO The first 2 bytes are always 0. The third byte codes the type of the data
    if (magic_number != 0x00000803) {
      throw std::runtime_error("invalid magic_number");
    }

    uint32_t number_of_images;
    reader_base.read_number(number_of_images);

    uint32_t number_of_rows;
    reader_base.read_number(number_of_rows);

    uint32_t number_of_columns;
    reader_base.read_number(number_of_columns);

    images.resize(number_of_images);
    for (size_t number_of_image{}; number_of_image < number_of_images; ++number_of_image) {
      auto& image = images.at(number_of_image);
      image.pixels.resize(number_of_rows * number_of_columns);
      for (size_t ind{}; ind < number_of_rows * number_of_columns; ++ind) {
        uint8_t pixel;
        reader_base.read_number(pixel);
        image.pixels.at(ind) = pixel;
      }
    }

    if (reader_base.size()) {
      throw std::runtime_error("invalid size");
    }
  }

  void read_images_label(images_t& images, const std::string& path) {
    reader_base_t reader_base;
    reader_base.init(path);

    uint32_t magic_number;
    reader_base.read_number(magic_number);
    if (magic_number != 0x00000801) {
      throw std::runtime_error("invalid magic_number");
    }

    uint32_t number_of_images;
    reader_base.read_number(number_of_images);

    images.resize(number_of_images);
    for (size_t number_of_image{}; number_of_image < number_of_images; ++number_of_image) {
      auto& image = images.at(number_of_image);
      uint8_t label;
      reader_base.read_number(label);
      image.label = label;
    }

    if (reader_base.size()) {
      throw std::runtime_error("invalid size");
    }
  }
};


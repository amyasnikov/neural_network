
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "matrix.h"

TEST_CASE("matrix is computed", "[matrix]") {
  matrix_t m1(2, {0, 1,  1, 2});
  matrix_t m2(2, {1, 1,  1, 0});
  matrix_t m3(4, {1, 1, 0, 0,  1, 2, 0, 1});

  matrix_t m_add(2, {1, 2,  2, 2});
  matrix_t m_mult(4, {1, 2, 0, 1,  3, 5, 0, 2});
  matrix_t m_square(2, {0, 1,  1, 4});
  matrix_t m_row1(4, {1, 2, 0, 1});
  matrix_t m_column1(1, {1,  2});
  matrix_t m_transpose(2, {1, 1,  1, 2,  0, 0,  0, 1});

  REQUIRE((m1 + m2).show() == m_add.show());
  REQUIRE((m1 * m3).show() == m_mult.show());
  REQUIRE(m1.apply([](double x) { return x * x; }).show() == m_square.show());
  REQUIRE(m3.row(1).show() == m_row1.show());
  REQUIRE(m3.column(1).show() == m_column1.show());
  REQUIRE(m3.t().show() == m_transpose.show());

  // std::cout << (m1 * m3).show() << std::endl;
}

/*TEST_CASE( "vectors can be sized and resized", "[vector]" ) {

  std::vector<int> v( 5 );

  REQUIRE( v.size() == 5 );
  REQUIRE( v.capacity() >= 5 );

  SECTION( "resizing bigger changes size and capacity" ) {
    v.resize( 10 );

    REQUIRE( v.size() == 10 );
    REQUIRE( v.capacity() >= 10 );
  }
  SECTION( "resizing smaller changes size but not capacity" ) {
    v.resize( 0 );

    REQUIRE( v.size() == 0 );
    REQUIRE( v.capacity() >= 5 );
  }
  SECTION( "reserving bigger changes capacity but not size" ) {
    v.reserve( 10 );

    REQUIRE( v.size() == 5 );
    REQUIRE( v.capacity() >= 10 );
  }
  SECTION( "reserving smaller does not change size or capacity" ) {
    v.reserve( 0 );

    REQUIRE( v.size() == 5 );
    REQUIRE( v.capacity() >= 5 );
  }
}*/


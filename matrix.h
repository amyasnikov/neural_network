
#include <valarray>
#include <sstream>

struct matrix_t {
  using value_t = double;

  size_t dim;
  std::valarray<value_t> data;

  matrix_t(const matrix_t& matrix) : dim(matrix.dim), data(matrix.data) { }

  matrix_t(size_t m, size_t n) : dim(n), data(m * n) { }

  matrix_t(size_t dim, std::initializer_list<value_t> l) : dim(dim), data(l) {
    if (m() * n() != data.size())
      throw std::runtime_error("matrix: invalid dim");
  }

  double& at(int x, int y) { return data[x * dim + y]; }
  const double& at(int x, int y) const { return data[x * dim + y]; }

  size_t m() const { return data.size() / dim; }
  size_t n() const { return dim; }

  std::string show() const {
    std::stringstream ss;
    ss << "{dim: " << dim << ", data: [";
    for (size_t i{}; i < data.size(); ++i) {
      ss << data[i] << ", ";
      if ((i % dim) == dim - 1)
        ss << "  ";
    }
    ss << "]}";
    return ss.str();
  }

  value_t sum() const {
    return data.sum();
  }

  matrix_t operator+(const matrix_t& matrix) const {
    if (m() != matrix.m() || n() != matrix.n())
      throw std::runtime_error("operator+: invalid dim");
    matrix_t ret(m(), n());
    ret.data = data + matrix.data;
    return ret;
  }

  matrix_t operator-(const matrix_t& matrix) const {
    if (m() != matrix.m() || n() != matrix.n())
      throw std::runtime_error("operator-: invalid dim");
    matrix_t ret(m(), n());
    ret.data = data - matrix.data;
    return ret;
  }

  matrix_t operator*(const matrix_t& matrix) const {
    if (n() != matrix.m())
      throw std::runtime_error("operator*: invalid dim");

    matrix_t ret(m(), matrix.n());
    for (size_t i{}; i < ret.m(); ++i) {
      for (size_t j{}; j < ret.n(); ++j) {
        ret.at(i, j) = 0;
        for (size_t k{}; k < n(); ++k) {
          ret.at(i, j) += at(i, k) * matrix.at(k, j);
        }
      }
    }

    return ret;
  }

  matrix_t mult_nativ(const matrix_t& matrix) const {
    if (m() != matrix.m() || n() != matrix.n())
      throw std::runtime_error("mult_nativ: invalid dim");
    matrix_t ret(m(), n());
    ret.data = data * matrix.data;
    return ret;
  }

  matrix_t apply(value_t func(value_t)) const {
    matrix_t matrix(*this);
    for (value_t& v : matrix.data) {
      v = func(v);
    }
    return matrix;
  }

  matrix_t apply(value_t func(const value_t&)) const {
    matrix_t matrix(*this);
    for (value_t& v : matrix.data) {
      v = func(v);
    }
    return matrix;
  }

  matrix_t row(size_t x) const {
    if (x >= m())
      throw std::runtime_error("row: invalid dim");
    matrix_t matrix(1, n());
    matrix.data = data[std::slice(x * n(), n(), 1)];
    return matrix;
  }

  matrix_t column(size_t y) const {
    if (y >= n())
      throw std::runtime_error("column: invalid dim");
    matrix_t matrix(m(), 1);
    matrix.data = data[std::slice(y, m(), n())];
    return matrix;
  }

  matrix_t t() const {
    matrix_t matrix(n(), m());
    for (size_t i{}; i < m(); ++i) {
      for (size_t j{}; j < n(); ++j) {
        matrix.at(j, i) = at(i, j);
      }
    }
    return matrix;
  }
};


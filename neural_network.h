
#include <random>

#include <eigen3/Eigen/Dense>

struct neural_network_t {

  size_t inodes;
  size_t hnodes;
  size_t onodes;
  double lr;
  std::string name;

  Eigen::MatrixXd wih;
  Eigen::MatrixXd who;

  std::function<double(double)> activation_function = [](double x) { return x; };

  neural_network_t(const std::string& name, size_t inputnodes,
      size_t hiddennodes, size_t outputnodes, double learningrate)
    : name(name), inodes(inputnodes), hnodes(hiddennodes),
      onodes(outputnodes), lr(learningrate) {

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    std::normal_distribution<double> dist_input{0, std::pow(inodes, -0.5)};
    std::normal_distribution<double> dist_hidden{0, std::pow(hnodes, -0.5)};

    auto rnd_input  = [&dist_input, &gen]() { return dist_input(gen); };
    auto rnd_hidden = [&dist_hidden, &gen]() { return dist_hidden(gen); };

    wih = Eigen::MatrixXd::NullaryExpr(hnodes, inodes, rnd_hidden);
    who = Eigen::MatrixXd::NullaryExpr(onodes, hnodes, rnd_input);

    activation_function = [](double x) { return 1. / (1. + std::exp(-x)); };
  }

  void train_dataset(double eps, const std::vector<Eigen::VectorXd>& inputs_array, const std::vector<Eigen::VectorXd>& targets_array) {
    read();
    double error = eps;
    while (error >= eps) {
      error = 0;
      for (size_t ind{}; ind < inputs_array.size(); ++ind) {
        error += train(inputs_array.at(ind), targets_array.at(ind));
      }
      std::cout << "error: " << error << " / " << eps << std::endl;
      write();
    }
  }

  double train(const Eigen::VectorXd& inputs, const Eigen::VectorXd& targets) {
    auto hidden_inputs = wih * inputs;
    auto hidden_outputs = hidden_inputs.unaryExpr(activation_function);

    auto final_inputs = who * hidden_outputs;
    auto final_outputs = final_inputs.unaryExpr(activation_function);

    auto output_errors = targets - final_outputs;
    auto hidden_errors = who.transpose() * output_errors;

    who += lr * output_errors.cwiseProduct(final_outputs).cwiseProduct(Eigen::MatrixXd::Ones(final_outputs.rows(), final_inputs.cols()) - final_outputs) * hidden_outputs.transpose();
    wih += lr * hidden_errors.cwiseProduct(hidden_outputs).cwiseProduct(Eigen::MatrixXd::Ones(hidden_outputs.rows(), hidden_outputs.cols()) - hidden_outputs) * inputs.transpose();

    auto square = [](double x) { return x * x; };
    double error = output_errors.unaryExpr(square).sum();
    return error;
  }

  Eigen::VectorXd query(const Eigen::VectorXd& inputs) {
    auto hidden_inputs  = wih * inputs;
    auto hidden_outputs = hidden_inputs.unaryExpr(activation_function);

    auto final_inputs  = who * hidden_outputs;
    auto final_outputs = final_inputs.unaryExpr(activation_function);

    return final_outputs;
  }

  void write() {
    std::string fname = name;
    std::string fname_tmp = name + ".tmp";
    std::ofstream fout(fname_tmp);
    if (fout.is_open()) {
      fout << inodes << std::endl;
      fout << hnodes << std::endl;
      fout << onodes << std::endl;
      fout << lr << std::endl;
      for (size_t i{}; i < wih.rows(); ++i) {
        for (size_t j{}; j < wih.cols(); ++j) {
          fout << wih(i, j) << " ";
        }
        fout << std::endl;
      }
      for (size_t i{}; i < who.rows(); ++i) {
        for (size_t j{}; j < who.cols(); ++j) {
          fout << who(i, j) << " ";
        }
        fout << std::endl;
      }
    }
    std::rename(fname_tmp.c_str(), fname.c_str());
  }

  void read() {
    std::ifstream fin(name);
    if (fin.is_open()) {
      fin >> inodes;
      fin >> hnodes;
      fin >> onodes;
      fin >> lr;
      wih = Eigen::MatrixXd(hnodes, inodes);
      who = Eigen::MatrixXd(onodes, hnodes);
      double val;
      for (size_t i{}; i < wih.rows(); ++i) {
        for (size_t j{}; j < wih.cols(); ++j) {
          fin >> val;
          wih(i, j) = val;
        }
      }
      for (size_t i{}; i < who.rows(); ++i) {
        for (size_t j{}; j < who.cols(); ++j) {
          fin >> val;
          who(i, j) = val;
        }
      }
    }
  }
};

